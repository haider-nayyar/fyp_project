<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use Auth;
use Redirect;
class ProfileController extends Controller
{

    function index(){
        $profile = User::find(Auth::user()->id)->profile;

        return view('profile',compact('profile'));
    }
    function profileSettings(){
        $profile = User::findorfail(Auth::user()->id)->profile;

        return view('profileSettings',compact('profile'));
    }

    function saveSettings(Request $request){

        $profile = Profile::find(Auth::user()->id);

        if($profile ==  null){
            $profile = new Profile();
        }

        if($request->country == null){
            $profile->country = $request->filledCountry;
            $profile->city = $request->filledCity;
            $profile->about = $request->filledAbout;
            $profile->user_id = Auth::user()->id;
            $profile->save();


        }else{
            $profile->country = $request->country;
            $profile->city = $request->city;
            $profile->about = $request->about;
            $profile->user_id = Auth::user()->id;
            $profile->save();
        }


        $user = User::findorfail(Auth::user()->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        $success = "Saved";

        return Redirect::to('/profile/profileSettings')->with('success', 'Saved');
    }
}
