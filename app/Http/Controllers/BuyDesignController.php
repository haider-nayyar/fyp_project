<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BuyDesignController extends Controller
{
    public function index($id){

        return view('buyDesign')->with('id',$id);
    }
}
