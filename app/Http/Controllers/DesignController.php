<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Design;
class DesignController extends Controller
{
    //


    function index(){
        return view('design');
    }
    function create(Request $request){
        $design = new Design();
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('designs'), $imageName);
        $design->title = $request->title;
        $design->description = $request->description;
        $design->image = $imageName;
        $design->price = $request->price;
        $design->save();

        return redirect('\home');
    }
}
