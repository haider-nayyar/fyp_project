<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Design;
class ProductInsideController extends Controller
{
    function index($id){
        $design = Design::findorfail($id);
        return view('productInside',compact('design'));
    }
}
