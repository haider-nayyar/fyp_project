<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $fillable = ['country', 'city', 'about'];

    function user(){
        return $this->belongsTo('App\User');
    }
}
