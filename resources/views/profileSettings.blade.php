@extends('layouts.app')
<style>
    .main-section{
        border:1px solid #138496;
        background-color: #fff;
    }
    .profile-header{
        background-color: #17a2b8;
        height:150px;
    }
    .user-detail{
        margin:-50px 0px 30px 0px;
    }
    .user-detail img{
        height:100px;
        width:100px;
    }
    .user-detail h5{
        margin:15px 0px 5px 0px;
    }
    .user-social-detail{
        padding:15px 0px;
        background-color: #17a2b8;
    }
    .user-social-detail a i{
        color:#fff;
        font-size:23px;
        padding: 0px 5px;
    }
</style>

@section('content')
    <div class="container-fluid">
        <!-- template-->
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="row">
                    <div class=" col-lg-12 col-md-12 main-section text-center">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-12 profile-header"></div>
                        </div>
                        <div class="row user-detail">
                            @if(is_null($profile))
                                <div class="col-lg-12 col-sm-12 col-12">
                                    <img src="{{asset('img/mens-t-shirt-mockup.jpg')}}" class="rounded-circle img-thumbnail">
                                    <h5>{{Auth::user()->name}}</h5>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i><a href="{{route('profileSettings')}}" class="btn btn-outline-primary m-1">Add Country and City</a> </p>

                                    <hr>
                                    <a href="#" class="btn btn-info btn-sm">Send Messege</a>

                                    <hr>
                                    <a href="{{route('profileSettings')}}">
                                        <span class="btn btn-outline-primary">Add Bio</span>
                                    </a>

                                </div>
                            @else
                                <div class="col-lg-12 col-sm-12 col-12">
                                    <img src="{{asset('img/mens-t-shirt-mockup.jpg')}}" class="rounded-circle img-thumbnail">
                                    <h5>{{Auth::user()->name}}</h5>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{$profile->city}}, {{$profile->country}}.</p>

                                    <hr>
                                    <a href="#" class="btn btn-info btn-sm">Send Messege</a>

                                    <hr>
                                    <span>{{$profile->about}}</span>
                                </div>
                            @endif
                        </div>
                        <div class="row user-social-detail">
                            <div class="col-lg-12 col-sm-12 col-12">
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">


                    <nav class="col-md-12 d-none d-md-block bg-light sidebar">
                        <div class="sidebar-sticky">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('profile')}}" data-target="#mydesigns" >
                                        <span data-feather="home"></span>
                                        My Designs
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-target="#purchaseddesigns">
                                        <span data-feather="file"></span>
                                        Purchased Designs
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <span data-feather="shopping-cart"></span>
                                        Products
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <span data-feather="users"></span>
                                        Customers
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <span data-feather="bar-chart-2"></span>
                                        Reports
                                    </a>
                                </li>

                            </ul>

                            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                                <span>Settings</span>
                                <a class="d-flex align-items-center text-muted" href="#">
                                    <span data-feather="settings"></span>
                                </a>
                            </h6>
                            <ul class="nav flex-column mb-2">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">
                                        <span data-feather="user"></span>
                                        Profile Settings <span class="sr-only">(current)</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <span data-feather="file-text"></span>
                                        Account Settings
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </nav>


                </div>
            </div>
            <div class="col-md-9 ml-sm-auto col-lg-9 pt-3 px-4">
                @if(Session::get('success') != null)
                    <div class="d-flex col-md-10 align-items-center justify-content-center">
                        <div class="alert alert-success alert-dismissible fade show col-md-4">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-center">{{Session::get('success')}}</p>
                        </div>
                    </div>
                @endif
                <main role="main" class="col-md-10 ">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="text-center">Profile Settings</h5>
                        </div>
                        <div class="body d-flex justify-content-center">
                            <form role="form" class="form pt-3 col-md-11" action="{{route('saveSettings')}}" method="post">
                               {{csrf_field()}}
                                <div class="form-group p-1">
                                    <input class="form-control" type="text" name="name" id="name"  placeholder="Name" value="{{Auth::User()->name}}">
                                </div>
                                <div class="form-group p-1">
                                    <input class="form-control" type="text" name="email" id="email"  placeholder="Email" value="{{Auth::User()->email}}">
                                </div>
                                @if(!$profile == null)
                                <div class="form-group p-1">
                                    <input class="form-control" type="text" name="filledCountry" id="filledCountry"  placeholder="Country" value="{{$profile->country}}">
                                </div>
                                <div class="form-group p-1">
                                    <input class="form-control" type="text" name="filledCity"  placeholder="City" id="filledCity" value="{{$profile->city}}">
                                </div>
                                <div class="form-group p-1">
                                    <textarea class="form-control" type="text" name="filledAbout" id="filledAbout"  placeholder="About">{{$profile->about}}</textarea>
                                </div>
                                    @else
                                    <div class="form-group p-1">
                                        <input class="form-control" type="text" name="country"  placeholder="Country" >
                                    </div>
                                    <div class="form-group p-1">
                                        <input class="form-control" type="text" name="city"  placeholder="City">
                                    </div>
                                    <div class="form-group p-1">
                                        <textarea class="form-control" type="text" name="about"  placeholder="About"></textarea>
                                    </div>
                                @endif
                                    <div class="form-group p-1 d-flex justify-content-center">
                                        <input class="form-control btn btn-success col-md-8" type="submit"  value="Save">
                                    </div>



                            </form>
                        </div>
                    </div>

                </main>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../../../assets/js/vendor/popper.min.js"></script>
    <script src="../../../../dist/js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
        feather.replace()
    </script>

    <!-- Graphs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script>
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                datasets: [{
                    data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                    lineTension: 0,
                    backgroundColor: 'transparent',
                    borderColor: '#007bff',
                    borderWidth: 4,
                    pointBackgroundColor: '#007bff'
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                },
                legend: {
                    display: false,
                }
            }
        });
    </script>
@endsection
