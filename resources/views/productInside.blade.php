@extends('layouts.app')
@section('content')

    {{--//stripe chcekout api--}}
    {{--<script src="https://checkout.stripe.com/checkout.js"></script>--}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 ml-5 mt-5">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$design->title}}</h5>
                    </div>
                    <img src="{{asset('designs/'.$design->image)}}" class="card-img-top"/>
                    <div class="card-body">
                        <div class=" btn btn-outline-primary p-3   m-2 col-md-12">Price: {{$design->price}}</div>
                        <h5 class="card-text col-md-12">Description</h5>
                        <p class="card-text text-justify col-md-12">
                            {{$design->description}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5">
                @if($design->id == Auth::User()->id)


                @else
                <div class="card">
                    <div class="body">
                        <ul class="list-group">
                            <li class="list-group-item">Design price : Rs. {{$design->price}}</li>
                            <li class="list-group-item">Other Charges : Rs. 0</li>
                            <li class="list-group-item">Total : Rs. {{$design->price}}</li>
                            {{--<li class="list-group-item"><a class="btn btn-primary text-white col-md-12" id="payment_button">--}}
                                    {{--Buy Design--}}
                                {{--</a></li>--}}

                            <li class="list-group-item">
                                <form action="/make-payment" method="POST">
                                    {{ csrf_field() }}
                                   
                                    <script
                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="{{ config('services.stripe.key') }}"
                                            data-amount="1000"
                                            data-name="Demo Book"
                                            data-description="This is good start up book."
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-locale="auto">
                                    </script>
                                </form>
                            </li>
                        </ul>


                    </div>
                </div>
                @endif

            </div>
        </div>

    </div>

    //Stripe
    {{--<script>--}}
        {{--var handler = StripeCheckout.configure({--}}
            {{--key: 'pk_test_My36Amqpjtz9G3QtIYbSKy2v',--}}
            {{--image: 'https://stripe.com/img/documentation/checkout/marketplace.png',--}}
            {{--locale: 'auto',--}}
            {{--token: function(token) {--}}
                {{--// You can access the token ID with `token.id`.--}}
                {{--// Get the token ID to your server-side code for use.--}}
            {{--}--}}
        {{--});--}}

        {{--document.getElementById('payment_button').addEventListener('click', function(e) {--}}
            {{--// Open Checkout with further options:--}}
            {{--handler.open({--}}
                {{--name: 'h3fyp',--}}
                {{--description: '2 widgets',--}}
                {{--amount: 2000--}}
            {{--});--}}
            {{--e.preventDefault();--}}
        {{--});--}}

        {{--// Close Checkout on page navigation:--}}
        {{--window.addEventListener('popstate', function() {--}}
            {{--handler.close();--}}
        {{--});--}}
    {{--</script>--}}
@endsection