@extends('layouts.app')
@section('content')
    <div class="jumbotron top-header-card">
        <a href="{{route('design')}}"  class="btn btn-danger p-3 float-right col-md-2 header-btn" style="margin-left:38rem">Start Designing</a>
    </div>
<div class="container-fluid">

    <div class="row">
    <div class="col-md-3">
        <div class="sidebar-card float-left" style="width: 18rem">
            <div class="card">
                <h5 class="card-header text-center">
                   Recent views
                </h5>
                <div class="card-body">
                    <div class="card mb-4">
                        <img src="{{asset('img/mens-t-shirt-mockup.jpg')}}" class="card-img-top">
                        <div class="card-body">
                            <strong class="card-title">This is some title</strong>
                            <p class="card-text">This is some text</p>
                        </div>
                    </div>
                    <div class="card mb-4">
                        <img src="{{asset('img/mens-t-shirt-mockup.jpg')}}" class="card-img-top">
                        <div class="card-body">
                            <strong class="card-title">This is some title</strong>
                            <p class="card-text">This is some text</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-9">
        @if($designs->count() > 0)
        <h5 class="category-text">Designs in T-Shirts</h5>

    <div class="row">
        @foreach($designs as $design)
        <div class="content-card" style="width: 14rem">
            <a class="card card-link" href="{{route('productInside',$design->id)}}">
                <img src="{{asset('designs/'.$design->image)}}" class="card-img-top"/>
                <div class="card-body">
                    <strong class="card-title">{{$design->title}}</strong>
                    <p class="card-text">{{$design->description}}</p>
                </div>
                <div class="card-footer">
                    <p class="float-right">price: <strong>{{$design->price}}</strong></p>
                </div>
            </a>
        </div>
        @endforeach
    </div>
        @endif

        {{--<h5 class="category-text">Designs in T-Shirts</h5>--}}
        {{--<div class="row">--}}
            {{--<div class="content-card" style="width: 14rem">--}}
                {{--<div class="card">--}}
                    {{--<img src="{{asset('img/mens-t-shirt-mockup.jpg')}}" class="card-img-top"/>--}}
                    {{--<div class="card-body">--}}
                        {{--<strong class="card-title">This is some title</strong>--}}
                        {{--<p class="card-text">This is some description</p>--}}
                    {{--</div>--}}
                    {{--<div class="card-footer">--}}
                        {{--<p class="float-right">price: <strong>$10</strong></p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="content-card" style="width: 14rem">--}}
                {{--<div class="card">--}}
                    {{--<img src="{{asset('img/mens-t-shirt-mockup.jpg')}}" class="card-img-top"/>--}}
                    {{--<div class="card-body">--}}
                        {{--<strong class="card-title">This is some title</strong>--}}
                        {{--<p class="card-text">This is some description</p>--}}
                    {{--</div>--}}
                    {{--<div class="card-footer">--}}
                        {{--<p class="float-right">price: <strong>$10</strong></p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="content-card" style="width: 14rem">--}}
                {{--<div class="card">--}}
                    {{--<img src="{{asset('img/mens-t-shirt-mockup.jpg')}}" class="card-img-top"/>--}}
                    {{--<div class="card-body">--}}
                        {{--<strong class="card-title">This is some title</strong>--}}
                        {{--<p class="card-text">This is some description</p>--}}
                    {{--</div>--}}
                    {{--<div class="card-footer">--}}
                        {{--<p class="float-right">price: <strong>$10</strong></p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="content-card" style="width: 14rem">--}}
                {{--<div class="card">--}}
                    {{--<img src="{{asset('img/mens-t-shirt-mockup.jpg')}}" class="card-img-top"/>--}}
                    {{--<div class="card-body">--}}
                        {{--<strong class="card-title">This is some title</strong>--}}
                        {{--<p class="card-text">This is some description</p>--}}
                    {{--</div>--}}
                    {{--<div class="card-footer">--}}
                        {{--<p class="float-right">price: <strong>$10</strong></p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
    </div>
</div>
@endsection
<style>
    .header-btn{
        margin-top:16rem;
    }

    .top-header-card{
        background-image:url("{{asset('img/banner1.jpg')}}");
        background-repeat: no-repeat;
        height:400px;
        width:100%;
        background-size: cover;
    }
    .content-card{
        margin:0.3rem;
    }
    .content-card a{
        color:black;
    }
    .sidebar-card{
        margin:0.3rem;
    }
    .category-text{
        margin-top:1rem;
        color:#343a40;
    }
</style>
