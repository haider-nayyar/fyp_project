<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name= "viewport" content = "width = device-width, initial-scal=1, shrink-to-fit=no">
    <meta http-equiv = "x-ua-compatible" content = "ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-4.0.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/animate.min.css">


    <title>H3 FYP</title>

</head>
<body>
<header>
    <div id="container">
        <div id="logo">
            <img class= "animated infinite bounce" src="../img/FYP.png" width="100px">

            <nav>
                @if (Route::has('login'))
                    <ul>
                        @if (Auth::check())
                            <li><a href="{{url('/home')}}">Home</a> </li>
                        @else
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{url('/register')}}">Register</a> </li>
                        @endif
                    </ul>
                @endif
            </nav>
        </div>
        <div id="prods">
            <nav>
                <ul>
                    <li><a href="../img/Shirts.html">Shirts</a></li>
                    <li><a href="../img/Shoes.html">Shoes</a></li>
                    <li><a href="../img/Mugs.html">Mugs</a></li>
                </ul>
            </nav>

        </div>

    </div>

</header>
<main role="main">

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide" src="../img/design.jpg" alt="First slide" width="device-width">
                <div class="container">
                    <div class="carousel-caption text-left">
                        <h1>Empowering Graphic Desingers</h1>
                        <p>Buy beautifully designed products by talented graphic desginers around the world</p>
                        <p><a class="btn btn-lg btn-primary" href="about.html" role="button">Learn More</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="second-slide" src="../img/design2.jpg" alt="Second slide"
                     width="device-width">
                <div class="container">
                    <div class="carousel-caption">
                        <h1> Design, earn, Be Free! </h1>
                        <p>Join us now and start your career now! </p>
                        <p><a class="btn btn-lg btn-primary" href="signup.html" role="button">Sign Up</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="third-slide" src="../img/stock.jpg" alt="Third slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h1>Search hundreds of amazing products </h1>
                        <p>Lorem Ipsum.........................</p>
                        <p><form>

                            <input type="text" id="text" class="form-control" placeholder="Search Products" required autofocus>
                            <a href="#" class="btn btn-primary my-2">Search</a>
                        </form></p>
                    </div>
                </div>
            </div>
        </div>

        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>



    <footer>
        <p>FYP DDP-FA14-BCS-A, Copyright &copy; 2018</p>
    </footer>
</main>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../css/bootstrap-4.0.0/assets/js/vendorjquery-slim.min.js"><\/script>')</script>
<script src="../css/bootstrap-4.0.0/assets/js/vendor/popper.min.js"></script>
<script src="../css/bootstrap-4.0.0/dist/js/bootstrap.min.js"></script>
</body>
</html>
