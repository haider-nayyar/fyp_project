@extends('layouts.app');
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4">
            <span class="anchor" id="formPayment"></span>

            <!-- form card cc payment -->
            <div class="card card-outline-secondary">
                <div class="card-body">
                    <h3 class="text-center">Upload your Design</h3>
                    <hr>
                    {{--<div class="alert alert-info p-2 pb-3">--}}
                        {{--<a class="close font-weight-normal initialism" data-dismiss="alert" href="#"><samp>&times;</samp></a>--}}
                        {{--CVC code is required.--}}
                    {{--</div>--}}
                    <form class="form" method="post" role="form" autocomplete="on" enctype="multipart/form-data" action="{{route('createDesign')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title"  title="Title of Design" required="required">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea type="text" id="description" name="description" class="form-control" cols="15" rows="5" required=""></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="file" class="form-control" autocomplete="off" maxlength="3"  title="Images to upload" required="" placeholder="image" id="image" name="image">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-12" for="price">Price</label>
                        </div>
                        <div class="form-inline">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">Rs.</span></div>
                                <input type="text" class="form-control text-right" id="price" name="price" placeholder="39">
                                <div class="input-group-append"><span class="input-group-text">.00</span></div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <button type="reset" class="btn btn-default btn-lg btn-block">Cancel</button>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success btn-lg btn-block">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /form card cc payment -->

        </div>


    </div>
</div>
@endsection

