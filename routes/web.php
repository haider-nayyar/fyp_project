<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/design',function(){
    return view('shirtdesigner');
});
Route::get('/profile','ProfileController@index')->name('profile');
Route::get('/profile/profileSettings','ProfileController@profileSettings')->name('profileSettings');
Route::post('/profile/profileSettings/saving','ProfileController@saveSettings')->name('saveSettings');
Route::get('/product/{id}','ProductInsideController@index')->name('productInside');

Route::get('/design','DesignController@index')->name('design');
Route::post('/design/store','DesignController@create')->name('createDesign');
Route::get('/design/buy/{id}','BuyDesignController@index')->name('buy');

//payment route
Route::post('/make-payment', 'PaymentsController@pay');


